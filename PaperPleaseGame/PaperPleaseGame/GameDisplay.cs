﻿// <copyright file="GameDisplay.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PaperPleaseGame
{
    using System;
    using System.Globalization;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// GameDisplay class.
    /// </summary>
    public class GameDisplay
    {
        private static Random random = new Random();
        private GameModel gameModel;
        private GeometryDrawing immigrantDrawing;
        private GeometryDrawing background;
        private GeometryDrawing desk;
        private GeometryDrawing documentDrawing;
        private FormattedText textCountry;
        private FormattedText textDateOfBirth;
        private FormattedText textDateOfExpired;
        private FormattedText textGender;
        private FormattedText textPersonId;
        private FormattedText textName;
        private FormattedText textEthnic;
        private DrawingGroup allowButton;
        private DrawingGroup denyButton;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameDisplay"/> class.
        /// the constructor.
        /// </summary>
        /// <param name="gameModel">the GameModel param.</param>
        public GameDisplay(GameModel gameModel)
        {
            this.gameModel = gameModel;
            this.background = new GeometryDrawing(
                Config.BackGroundColor,
                new Pen(Config.BorderColor, Config.BorderThickness),
                new RectangleGeometry(Config.GameWindow));
            this.desk = new GeometryDrawing(
                Config.DeskColor,
                new Pen(Brushes.BurlyWood, 2),
                new RectangleGeometry(Config.Desk));

            this.allowButton = this.ButtonBuild(" Allow NPC", 600);
            this.denyButton = this.ButtonBuild(" Deny NPC", 300);
        }

        /// <summary>
        /// Gets or sets allowButton.
        /// </summary>
        public DrawingGroup AllowButton { get => this.allowButton; set => this.allowButton = value; }

        /// <summary>
        /// Gets or sets denyButton.
        /// </summary>
        public DrawingGroup DenyButton { get => this.denyButton; set => this.denyButton = value; }

        /// <summary>
        /// Drawing all the Image for the display.
        /// </summary>
        /// <param name="drawingContext">DrawingContext.</param>
        public void DrawingImages(DrawingContext drawingContext)
        {
            FormattedText money =
                new FormattedText("Money: " + this.gameModel.Money.ToString(), CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Cambria"), 15, Brushes.Black);
            GeometryDrawing moneyDrawing =
                new GeometryDrawing(Brushes.Black, null, money.BuildGeometry(new Point(10, 10)));
            FormattedText level =
                new FormattedText("Day: " + this.gameModel.Day.ToString() + " Date: " + this.gameModel.DateTimeToday.ToString(), CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Cambria"), 15, Brushes.Black);
            GeometryDrawing levelDrawing =
                new GeometryDrawing(Brushes.Black, null, level.BuildGeometry(new Point(100, 10)));

            DrawingGroup drawingGroup = new DrawingGroup();
            this.immigrantDrawing =
                new GeometryDrawing(this.gameModel.CurrentImmigrant.GetNPCBrush(), null, new RectangleGeometry(new Rect(0, Config.Height, 200, 400)));
            drawingGroup.Children.Add(this.background);
            drawingGroup.Children.Add(this.desk);
            drawingGroup.Children.Add(this.immigrantDrawing);
            drawingGroup.Children.Add(this.BuildDocument(this.gameModel.CurrentImmigrant.Passport, 310));
            drawingGroup.Children.Add(this.BuildDocument(this.gameModel.CurrentImmigrant.ID, 710));
            drawingGroup.Children.Add(moneyDrawing);
            drawingGroup.Children.Add(levelDrawing);
            drawingGroup.Children.Add(this.DrawingCountry());
            drawingContext.DrawDrawing(drawingGroup);
        }

        private DrawingGroup ButtonBuild(string buttonName, int positionX)
        {
            FormattedText formattedText = new FormattedText(buttonName, CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Cambria"), 15, Brushes.Black);
            GeometryDrawing buttonDrawing = new GeometryDrawing(Brushes.White, new Pen(Brushes.Black, 2), new RectangleGeometry(new Rect(positionX - 10, 300, 90, 30)));
            GeometryDrawing geoName = new GeometryDrawing(Brushes.Black, null, formattedText.BuildGeometry(new Point(positionX - 10, 300)));
            DrawingGroup drawingGroup = new DrawingGroup();
            drawingGroup.Children.Add(buttonDrawing);
            drawingGroup.Children.Add(geoName);
            return drawingGroup;
        }

        private string GenerateSlang()
        {
            int dice = random.Next(0, 2);
            string slang = "Empty";
            switch (dice)
            {
                case 0: slang = "Fuck you Officer, Roman scum "; break;
                case 1: slang = "Sorry officer, I dont have any document"; break;
            }

            return slang;
        }

        /// <summary>
        /// Drawing the Passport.
        /// </summary>
        /// <param name="document">oocument of NPC.</param>
        /// <param name="positionX">position of the element.</param>
        /// <returns>the Passport.</returns>
        private DrawingGroup BuildDocument(Document document, int positionX)
        {
            if (document == null)
            {
                FormattedText slang = new FormattedText(
                    this.GenerateSlang(), CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Cambria"), 15, Brushes.Red);
                GeometryDrawing slangPaper = new GeometryDrawing(Brushes.White, new Pen(Brushes.Black, 2), new RectangleGeometry(new Rect(positionX - 10, Config.Height + 100, 300, 150)));
                GeometryDrawing slangGeo = new GeometryDrawing(Brushes.Black, null, slang.BuildGeometry(new Point(positionX, Config.Height + 160)));

                DrawingGroup empty = new DrawingGroup();
                empty.Children.Add(slangPaper);
                empty.Children.Add(slangGeo);
                return empty;
            }

            this.documentDrawing = new GeometryDrawing(document.GetNPCBrush(), new Pen(Brushes.Black, 2), new RectangleGeometry(new Rect(positionX - 100, Config.Height + 100, 100, 150)));
            DrawingGroup drawingGroup = new DrawingGroup();
            this.textCountry = new FormattedText(
               "Country: " + document.Country, CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Cambria"), 15, Brushes.Red);
            this.textDateOfBirth = new FormattedText(
               "Date Of Birth: " + document.DateOfBirth.ToString(), CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Cambria"), 15, Brushes.Red);
            this.textDateOfExpired = new FormattedText(
               "Expired Date: " + this.gameModel.CurrentImmigrant.Passport.ExpiredDate.ToString(), CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Cambria"), 15, Brushes.Red);
            this.textEthnic = new FormattedText(
              "Ethnic: " + document.Ethnic, CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Cambria"), 15, Brushes.Red);
            this.textGender = new FormattedText(
              "Gender: " + document.Gender, CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Cambria"), 15, Brushes.Red);
            this.textName = new FormattedText(
              "Name: " + document.Name, CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Cambria"), 15, Brushes.Red);
            this.textPersonId = new FormattedText(
                       "PersonId: " + document.PersonId, CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Cambria"), 15, Brushes.Red);
            GeometryDrawing thePaper = new GeometryDrawing(Brushes.White, new Pen(Brushes.Black, 2), new RectangleGeometry(new Rect(positionX - 10, Config.Height + 100, 300, 150)));
            GeometryDrawing geoPersonId = new GeometryDrawing(Brushes.Black, null, this.textPersonId.BuildGeometry(new Point(positionX, Config.Height + 100)));
            GeometryDrawing geoName = new GeometryDrawing(Brushes.Black, null, this.textName.BuildGeometry(new Point(positionX, Config.Height + 120)));
            GeometryDrawing geoGender = new GeometryDrawing(Brushes.Black, null, this.textGender.BuildGeometry(new Point(positionX, Config.Height + 140)));
            GeometryDrawing geoCountry = new GeometryDrawing(Brushes.Black, null, this.textCountry.BuildGeometry(new Point(positionX, Config.Height + 160)));
            GeometryDrawing geoDateOfBirth = new GeometryDrawing(Brushes.Black, null, this.textDateOfBirth.BuildGeometry(new Point(positionX, Config.Height + 180)));
            GeometryDrawing geoExpiredDate = new GeometryDrawing(Brushes.Black, null, this.textDateOfExpired.BuildGeometry(new Point(positionX, Config.Height + 200)));
            GeometryDrawing geoEthnic = new GeometryDrawing(Brushes.Black, null, this.textEthnic.BuildGeometry(new Point(positionX, Config.Height + 220)));
            drawingGroup.Children.Add(this.documentDrawing);
            drawingGroup.Children.Add(thePaper);
            drawingGroup.Children.Add(geoCountry);
            drawingGroup.Children.Add(geoPersonId);
            drawingGroup.Children.Add(geoName);
            drawingGroup.Children.Add(geoGender);
            drawingGroup.Children.Add(geoDateOfBirth);
            drawingGroup.Children.Add(geoExpiredDate);
            drawingGroup.Children.Add(geoEthnic);
            return drawingGroup;
        }

        private DrawingGroup DrawingCountry()
        {
            int row = 30;
            FormattedText manual =
                new FormattedText(" The Manual of the Empires ", CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Cambria"), 15, Brushes.Black);
            GeometryDrawing manualDrawing =
                new GeometryDrawing(Brushes.Black, null, manual.BuildGeometry(new Point(450, 10)));

            DrawingGroup drawingGroup = new DrawingGroup();
            GeometryDrawing thePaper
                = new GeometryDrawing(Brushes.White, new Pen(Brushes.Black, 2), new RectangleGeometry(new Rect(400, 10, 300, 200)));
            drawingGroup.Children.Add(thePaper);
            drawingGroup.Children.Add(manualDrawing);
            foreach (string item in this.gameModel.Countries)
            {
                FormattedText country =
                new FormattedText("  Empires: " + item, CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Cambria"), 15, Brushes.Black);
                GeometryDrawing countryDrawing =
                    new GeometryDrawing(Brushes.Black, null, country.BuildGeometry(new Point(400, row)));
                drawingGroup.Children.Add(countryDrawing);
                row = row + 20;
            }

            return drawingGroup;
        }
    }
}
