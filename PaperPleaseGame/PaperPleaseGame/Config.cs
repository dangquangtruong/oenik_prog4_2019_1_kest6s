﻿// <copyright file="Config.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PaperPleaseGame
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Config.
    /// </summary>
    public static class Config
    {
        /// <summary>
        /// Width.
        /// </summary>
        public const double Width = 1000;

        /// <summary>
        /// Height.
        /// </summary>
        public const double Height = 300;

        /// <summary>
        /// BorderThickness.
        /// </summary>
        public const int BorderThickness = 4;

        /// <summary>
        /// deskColor.
        /// </summary>
        private static Brush deskColor = Brushes.IndianRed;
        private static Brush backGroundColor = Brushes.AntiqueWhite;
        private static Rect gameWindow = new Rect(0, 0, Width, Height);
        private static Rect desk = new Rect(201, Config.Height, 800, 400);

        private static Brush borderColor = Brushes.Gray;

        /// <summary>
        /// Gets gameWindow.
        /// </summary>
        public static Rect GameWindow { get => gameWindow; }

        /// <summary>
        /// Gets desk.
        /// </summary>
        public static Rect Desk { get => desk; }

        /// <summary>
        /// Gets backGroundColor.
        /// </summary>
        public static Brush BackGroundColor { get => backGroundColor; }

        /// <summary>
        /// Gets deskColor.
        /// </summary>
        public static Brush DeskColor { get => deskColor; }

        /// <summary>
        /// Gets borderColor.
        /// </summary>
        public static Brush BorderColor { get => borderColor; }
    }
}
