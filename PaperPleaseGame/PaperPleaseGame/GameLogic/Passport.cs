﻿// <copyright file="Passport.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PaperPleaseGame
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Passport of the NPC.
    /// </summary>
    public class Passport : Document
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Passport"/> class.
        /// </summary>
        /// <param name="name">name.</param>
        /// <param name="expiredDate">expiredDate.</param>
        /// <param name="dateOfBirth">dateOfBirth.</param>
        /// <param name="country">country.</param>
        /// <param name="personId">personId.</param>
        /// <param name="imageFaces">imageFaces.</param>
        /// <param name="ethnic">ethni.</param>
        /// <param name="gender">gender.</param>
        public Passport(string name, DateTime expiredDate, DateTime dateOfBirth, string country, string personId, string imageFaces, string ethnic, string gender)
            : base(name, expiredDate, dateOfBirth, country, personId, imageFaces, ethnic, gender)
        {
        }
    }
}
