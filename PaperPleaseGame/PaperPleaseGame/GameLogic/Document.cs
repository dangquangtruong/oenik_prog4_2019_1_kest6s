﻿// <copyright file="Document.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PaperPleaseGame
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// the document of the NPC.
    /// </summary>
    public abstract class Document
    {
        private string name;
        private DateTime expiredDate;
        private DateTime dateOfBirth;
        private string country;
        private string personId;
        private string imageFaces;
        private string ethnic;
        private string gender;
        private Rect documentRect;

        /// <summary>
        /// Initializes a new instance of the <see cref="Document"/> class.
        /// </summary>
        /// <param name="name">name.</param>
        /// <param name="expiredDate">expiredDate.</param>
        /// <param name="dateOfBirth">dateOfBirth.</param>
        /// <param name="country">country.</param>
        /// <param name="personId">personId.</param>
        /// <param name="imageFaces">imageFaces.</param>
        /// <param name="ethnic">ethnic.</param>
        /// <param name="gender">gender.</param>
        protected Document(string name, DateTime expiredDate, DateTime dateOfBirth, string country, string personId, string imageFaces, string ethnic, string gender)
        {
            this.name = name;
            this.expiredDate = expiredDate;
            this.dateOfBirth = dateOfBirth; // fuck American calendar
            this.country = country;
            this.personId = personId;
            this.imageFaces = imageFaces;
            this.ethnic = ethnic;
            this.documentRect = new Rect(300, Config.Height + 100, 100, 150);
            this.gender = gender;
        }

        /// <summary>
        /// Gets or sets expiredDate.
        /// </summary>
        public DateTime ExpiredDate
        {
            get => this.expiredDate;
            set => this.expiredDate = value;
        }

        /// <summary>
        /// Gets or sets.
        /// </summary>
        public string Name { get => this.name; set => this.name = value; }

        /// <summary>
        /// Gets or sets.
        /// </summary>
        public DateTime DateOfBirth { get => this.dateOfBirth; set => this.dateOfBirth = value; }

        /// <summary>
        /// Gets or sets.
        /// </summary>
        public string Country { get => this.country; set => this.country = value; }

        /// <summary>
        /// Gets or sets.
        /// </summary>
        public string PersonId { get => this.personId; set => this.personId = value; }

        /// <summary>
        /// Gets or sets.
        /// </summary>
        public string ImageFaces { get => this.imageFaces; set => this.imageFaces = value; }

        /// <summary>
        /// Gets or sets.
        /// </summary>
        public string Ethnic { get => this.ethnic; set => this.ethnic = value; }

        /// <summary>
        /// Gets.
        /// </summary>
        public Rect DocumentRect { get => this.documentRect; }

        /// <summary>
        /// Gets or sets.
        /// </summary>
        public string Gender { get => this.gender; set => this.gender = value; }

        /// <summary>
        /// return the GetBrush from the string image.
        /// </summary>
        /// <returns>return the GetBrush.</returns>
        public Brush GetNPCBrush()
        {
            return this.GetBrush(this.imageFaces);
        }

        private Brush GetBrush(string fname)
        {
            ImageBrush ib = new ImageBrush(new BitmapImage(new Uri(fname, UriKind.Relative)));
            ib.TileMode = TileMode.Tile;
            ib.Viewport = this.documentRect;
            ib.ViewportUnits = BrushMappingMode.Absolute;
            return ib;
        }
    }
}
