﻿// <copyright file="GameModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PaperPleaseGame
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// GameModel class.
    /// </summary>
    public class GameModel
    {
        // private + public List<GameRule> CurrentRules
        private static readonly Random Random = new Random();
        private List<Immigrant> queueOfImmigrants;
        private Immigrant currentImmigrant; // change the currentImmigrant in logic class
        private List<string> countries = new List<string>()
        {
            "Imperium Romanum", "Dragon Empire", "Fouth Reich", "Persian Empire", "Mauryan Empire", "Russian Empire",
        };

        private DateTime dateTimeToday = DateTime.Parse("7/20/2019");
        private int day;
        private int money;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameModel"/> class.
        /// </summary>
        public GameModel()
        {
            this.day = 0;
            this.money = 0;
            this.BuildListOfImmigrant();
            this.currentImmigrant = this.queueOfImmigrants.First();
        }

        /// <summary>
        /// Gets or sets day.
        /// </summary>
        public int Day { get => this.day; set => this.day = value; }

        /// <summary>
        /// Gets queueOfImmigrants.
        /// </summary>
        public List<Immigrant> QueueOfImmigrants { get => this.queueOfImmigrants;  }

        /// <summary>
        /// Gets countries.
        /// </summary>
        public List<string> Countries { get => this.countries;  }

        /// <summary>
        /// Gets or sets currentImmigrant.
        /// </summary>
        public Immigrant CurrentImmigrant { get => this.currentImmigrant; set => this.currentImmigrant = value; }

        /// <summary>
        /// Gets or sets <see cref="Money"/>.
        /// </summary>
        public int Money { get => this.money; set => this.money = value; }

        /// <summary>
        /// Gets or sets dateTimeToday.
        /// </summary>
        public DateTime DateTimeToday { get => this.dateTimeToday; set => this.dateTimeToday = value; }

        /// <summary>
        /// take random element from the list of countries.
        /// </summary>
        /// <returns>the country.</returns>
        private string RandomCountry()
        {
            int empire = Random.Next(0, this.countries.Count());
            string empireOfEarth = this.countries.ElementAt(empire);
            return empireOfEarth;
        }

        /// <summary>
        /// Build the Queue of Immigrant.
        /// </summary>
        private void BuildListOfImmigrant()
        {
            this.queueOfImmigrants = new List<Immigrant>();
            this.queueOfImmigrants.Add(new Immigrant(
                false,
                "My Queen.jpg",
                new Passport("Margueritte Breuil", DateTime.Parse("10/10/2020"), DateTime.Parse("10/5/1789"), this.countries[0], "KEST6S", "My Queen.jpg", "Romanae", "female"),
                new ID_Card("Margueritte Breuil", DateTime.Parse("10/10/2020"), DateTime.Parse("10/5/1789"), this.countries[0], "KEST6S", "My Queen.jpg", "Romanae", "female")));
            this.queueOfImmigrants.Add(
                new Immigrant(
                    false,
                    "Fox.jpg",
                    new Passport("Fox Lofasz", DateTime.Parse($"9/14/2019"), DateTime.Parse("4/14/1980"), this.RandomCountry(), "asfx1", "Fox.jpg", "Persian", "Female"),
                    new ID_Card("Prostitute", DateTime.Parse("9/14/2019"), DateTime.Parse("4/14/1980"), this.RandomCountry(), "asfx1", "Fox.jpg", "Persian", "Female")));
            this.queueOfImmigrants.Add(new Immigrant(
                    false,
                    "Eu2.jpg",
                    new Passport("Bitch", DateTime.Parse("9/14/2019"), DateTime.Parse("4/14/1980"), this.countries.ElementAt(0), "kesas", "Gipsy1.jpg", "Gypsi", "Female"),
                    new ID_Card("Prostitute", DateTime.Parse("9/14/2019"), DateTime.Parse("4/14/1980"), this.countries.ElementAt(0), "kesas", "Eu1.jpg", "Gypsi", "Female")));
            this.queueOfImmigrants.Add(new Immigrant(
                    false,
                    "Gipsy1.jpg",
                    new Passport("Emma", DateTime.Parse("9/20/2019"), DateTime.Parse("4/20/1980"), this.countries.ElementAt(0), "GGWP", "Gipsy1.jpg", "Gypsi", "Female"),
                    new ID_Card("Emma", DateTime.Parse("9/20/2019"), DateTime.Parse("4/20/1980"), "Persian Empire", "GGWP", "Gipsy1.jpg", "Gypsi", "Female")));
            this.queueOfImmigrants.Add(new Immigrant(
                    false,
                    "Alter Ego.jpg",
                    new Passport("Alter", DateTime.Parse("9/14/2019"), DateTime.Parse("4/25/1980"), this.countries.ElementAt(1), "GayShit", "Alter Ego.jpg", "Aryan", "Female"),
                    new ID_Card("Alter", DateTime.Parse("9/14/2019"), DateTime.Parse("4/25/1980"), this.countries.ElementAt(1), "GayShit", "Alter Ego.jpg", "Aryan", "Female")));
            this.queueOfImmigrants.Add(new Immigrant(
                    true,
                    "Fate Graphite.jpg",
                    new Passport("Emma", DateTime.Parse("6/14/2019"), DateTime.Parse("4/30/1980"), this.countries.ElementAt(5), "Fate Graphite", "Fate Graphite.jpg", "Aryan", "Female"),
                    new ID_Card("Emma", DateTime.Parse("6/14/2019"), DateTime.Parse("4/30/1980"), this.countries.ElementAt(5), "Fate Graphite", "Fate Graphite.jpg", "Aryan", "Female")));
            this.queueOfImmigrants.Add(new Immigrant(
                    true,
                    "Waifu.jpg",
                    new Passport("Waifu", DateTime.Parse("4/14/2019"), DateTime.Parse("4/1/1980"), this.countries.ElementAt(3), "kesas", "Waifu.jpg", "Kurdish", "Female"),
                    new ID_Card("Waifu", DateTime.Parse("4/14/2019"), DateTime.Parse("4/1/1980"), this.countries.ElementAt(3), "kesas", "Waifu.jpg", "Kurdish", "Female")));
            this.queueOfImmigrants.Add(new Immigrant(
                    false,
                    "Baron.png",
                    new Passport("Baron", DateTime.Parse("4/14/2019"), DateTime.Parse("4/5/1980"), this.countries.ElementAt(2), "kesas", "Baron.png", "Indian", "Female"),
                    new ID_Card("Baron", DateTime.Parse("4/14/2019"), DateTime.Parse("4/5/1980"), this.countries.ElementAt(2), "kesas", "Baron.png", "Indian", "Female")));
            this.queueOfImmigrants.Add(new Immigrant(
                    false,
                    "Arthur Alter.jpg",
                    new Passport("Arthur", DateTime.Parse("11/11/2019"), DateTime.Parse("4/14/1980"), this.RandomCountry(), "kesas", "Arthur Alter.jpg", "Gypsy", "Female"),
                    new ID_Card("Arthur", DateTime.Parse("11/11/2019"), DateTime.Parse("4/14/1980"), this.countries.ElementAt(0), "kesas", "Eu1.jpg", "Gypsy", "Female")));
            this.queueOfImmigrants.Add(new Immigrant(
                    false,
                    "Lord Vader.jpg",
                    new Passport("Darth Vader", DateTime.Parse("4/14/2030"), DateTime.Parse("4/14/1980"), "Imperium Romanum", "Sith", "Lord Vader.jpg", "Sith", "Male"),
                    new ID_Card("Darth Vader", DateTime.Parse("4/14/2030"), DateTime.Parse("4/14/1980"), "Imperium Romanum", "Sith", "Lord Vader.jpg", "Sith", "Male")));
            this.queueOfImmigrants.Add(new Immigrant(
                    false,
                    "Emperor_Sidious.png",
                    new Passport("Emperor", DateTime.Parse("4/14/2019"), DateTime.Parse("4/14/1980"), "Imperium Romanum", "Sith", "Emperor_Sidious.png", "Sith", "Male"),
                    new ID_Card("Emperor", DateTime.Parse("4/14/2019"), DateTime.Parse("4/14/1980"), "Imperium Romanum", "Sith", "Emperor_Sidious.png", "Sith", "Male")));
            this.queueOfImmigrants.Add(new Immigrant(
                    true,
                    "DarthBane.jpg",
                    new Passport("Darth Bane", DateTime.Parse("4/14/2019"), DateTime.Parse("4/14/1980"), "Imperium Romanum", "Sith", "DarthBane.jpg", "Sith", "Male"),
                    new ID_Card("Darth Bane", DateTime.Parse("4/14/2019"), DateTime.Parse("4/14/1980"), "Imperium Romanum", "Sith", "DarthBane.jpg", "Sith", "Male")));
            this.queueOfImmigrants.Add(new Immigrant(
                    true,
                    "Anakin_Skywalker.png",
                    new Passport("Anakin Skywalker", DateTime.Parse("4/14/2019"), DateTime.Parse("4/14/1980"), this.RandomCountry(), "gfkas", "Anakin_Skywalker.png", "White Master", "Male"),
                    new ID_Card("Anakin Skywalker", DateTime.Parse("4/14/2019"), DateTime.Parse("4/14/1980"), this.RandomCountry(), "eefga", "Anakin_Skywalker.png", "White Master", "Male")));
            this.queueOfImmigrants.Add(new Immigrant(
                    true,
                    "Luke.png",
                    new Passport("Luke Skywalker", DateTime.Parse("4/14/2019"), DateTime.Parse("4/14/1980"), this.RandomCountry(), "png", "Luke.png", "Gypsi", "Female"),
                    new ID_Card("Luke Skywalker", DateTime.Parse("4/14/2019"), DateTime.Parse("4/14/1980"), this.RandomCountry(), "kesas", "Luke.png", "Gypsi", "Female")));
            this.queueOfImmigrants.Add(new Immigrant(
                true, "Berserk.jpg", null, null));
            this.queueOfImmigrants.Add(new Immigrant(
                false,
                "Murasaki.png",
                new Passport("Murasaki", DateTime.Parse("7/17/2019"), DateTime.Parse("7/31/1931"), this.countries[1], "Lord", "Murasaki.png", "Japanese", "Female"),
                new ID_Card("Murasaki", DateTime.Parse("7/17/2019"), DateTime.Parse("7/31/1931"), this.countries[1], "Lord", "Murasaki.png", "Japanese", "Female")));
            this.queueOfImmigrants.Add(new Immigrant(
               false,
               "Leonardo Da Vinci.jpg",
               new Passport("Leonardo Da Vinci", DateTime.Parse("8/8/2080"), DateTime.Parse("4/8/1910"), this.countries[0], "456gp", "Leonardo Da Vinci.jpg", "Romanus", "Female"),
               new ID_Card("Leonardo Da Vinci", DateTime.Parse("8 /8/2080"), DateTime.Parse("4/8/1910"), this.countries[0], "456gp", "Leonardo Da Vinci.jpg", "Romanus", "Female")));
            this.queueOfImmigrants.Add(new Immigrant(
                false,
                "Masadaverse_Dies_Irae_Marie_(Render).png",
                new Passport("Marie", DateTime.Parse("4/14/2019"), DateTime.Parse("4/14/1980"), this.countries[5], "png", "Masadaverse_Dies_Irae_Marie_(Render).png", "Romanus", "Female"),
                new ID_Card("Marie", DateTime.Parse("4/14/2019"), DateTime.Parse("4/14/1980"), this.countries[5], "kesas", "Masadaverse_Dies_Irae_Marie_(Render).png", "Romanus", "Female")));
            this.queueOfImmigrants.Add(new Immigrant(
                false,
                "Shisou.jpg",
                new Passport("Shisou", DateTime.Parse("4/14/2019"), DateTime.Parse("4/14/1980"), this.countries[4], "EmLA1", "Shisou.jpg", "Gypsi", "Female"),
                new ID_Card("Shisou", DateTime.Parse("4/14/2019"), DateTime.Parse("4/14/1980"), this.countries[4], "EmLA1", "Shisou.jpg", "Gypsi", "Female")));
            this.queueOfImmigrants.Add(new Immigrant(
                false,
                "Sonnenkind.jpg",
                new Passport("Rea Himuro", DateTime.Parse("4/14/2018"), DateTime.Parse("4/14/1980"), this.countries[5], "png", "Sonnenkind.jpg", "Gypsi", "Female"),
                new ID_Card("Rea Himuro", DateTime.Parse("4/14/2018"), DateTime.Parse("4/14/1980"), this.countries[5], "kesas", "Sonnenkind.jpg", "Gypsi", "Female")));
            this.queueOfImmigrants.Add(new Immigrant(
                false,
                "Maria.jpg",
                null,
                null));
            this.queueOfImmigrants.Add(new Immigrant(
                false,
                "Mercurius.jpg",
                new Passport("Mercurius", DateTime.Parse("4/14/2019"), DateTime.Parse("4/14/1980"), this.RandomCountry(), "Mercurius", "Mercurius.jpg", "Mercurius", "Male"),
                new ID_Card("Mercurius", DateTime.Parse("4/14/2019"), DateTime.Parse("4/14/1980"), this.RandomCountry(), "Mercurius", "Mercurius.jpg", "Mercurius", "Male")));
            this.queueOfImmigrants.Add(new Immigrant(
                false,
                "Bradamante.png",
                new Passport("Bradamante", DateTime.Parse("4/14/2017"), DateTime.Parse("4/14/1980"), this.RandomCountry(), "KESAA", "Bradamante.png", "Romanus", "Female"),
                new ID_Card("Bradamante", DateTime.Parse("4/14/2017"), DateTime.Parse("4/14/1980"), this.RandomCountry(), "KESAA", "Bradamante.png", "Romanus", "Female")));
            this.queueOfImmigrants.Add(new Immigrant(
               false,
               "Tamamo_Vitch.png",
               new Passport("Tamamo Vitch", DateTime.Parse("4/14/2040"), DateTime.Parse("4/14/1910"), this.countries[0], "Emperor", "Tamamo_Vitch.png", "Romanus", "Female"),
               new ID_Card("Tamamo Vitch", DateTime.Parse("4/14/2040"), DateTime.Parse("4/14/1910"), this.countries[0], "Emperor", "Tamamo_Vitch.png", "Romanus", "Female")));
        }
    }
}
