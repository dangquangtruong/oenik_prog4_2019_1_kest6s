﻿// <copyright file="Immigrant.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PaperPleaseGame
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// Immigrant.
    /// </summary>
    public class Immigrant
    {
         // private string gender; //might use in the future
        private bool contrabandOwnership;

        private string imageFaces;
        private Passport passport;
        private ID_Card iD;

        /// <summary>
        /// Initializes a new instance of the <see cref="Immigrant"/> class.
        /// </summary>
        /// <param name="contrabandOwnership">contrabandOwnership.</param>
        /// <param name="imageFaces">imageFaces.</param>
        /// <param name="passport">passport.</param>
        /// <param name="iD">iD.</param>
        public Immigrant(bool contrabandOwnership, string imageFaces, Passport passport, ID_Card iD)
        {
            this.contrabandOwnership = contrabandOwnership;
            this.imageFaces = imageFaces;
            this.passport = passport;
            this.iD = iD;
        }

        /// <summary>
        ///
        /// Gets or sets a value indicating whether. </summary>
        public bool ContrabandOwnership { get => this.contrabandOwnership; set => this.contrabandOwnership = value; }

        /// <summary>
        /// Gets ImageFaces.
        /// </summary>
        public string ImageFaces { get => this.imageFaces; }

        /// <summary>
        /// Gets passport.
        /// </summary>
        public Passport Passport { get => this.passport; }

        /// <summary>
        /// Gets or sets id.
        /// </summary>
        public ID_Card ID { get => this.iD; set => this.iD = value; }

        /// <summary>
        /// GetNPCBrush with the string.
        /// </summary>
        /// <returns>Brush.</returns>
        public Brush GetNPCBrush()
        {
            return this.GetBrush(this.imageFaces);
        }

        private Brush GetBrush(string fname)
        {
            ImageBrush ib = new ImageBrush(new BitmapImage(new Uri(fname, UriKind.Relative)));
            ib.TileMode = TileMode.Tile;
            ib.Viewport = new Rect(0, Config.Height, 200, 400);
            ib.ViewportUnits = BrushMappingMode.Absolute;
            return ib;
        }
    }
}
