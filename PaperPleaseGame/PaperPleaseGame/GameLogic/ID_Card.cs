﻿// <copyright file="ID_Card.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PaperPleaseGame
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// ID_Card.
    /// </summary>
    public class ID_Card : Document
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ID_Card"/> class.
        /// </summary>
        /// <param name="name">name.</param>
        /// <param name="expiredDate">expiredDate.</param>
        /// <param name="dateOfBirth">dateOfBirth.</param>
        /// <param name="country">country.</param>
        /// <param name="personId">personId.</param>
        /// <param name="imageFaces">imageFaces.</param>
        /// <param name="ethnic">ethnic.</param>
        /// <param name="gender">gender.</param>
        public ID_Card(string name, DateTime expiredDate, DateTime dateOfBirth, string country, string personId, string imageFaces, string ethnic, string gender)
            : base(name, expiredDate, dateOfBirth, country, personId, imageFaces, ethnic, gender)
        {
        }
    }
}
