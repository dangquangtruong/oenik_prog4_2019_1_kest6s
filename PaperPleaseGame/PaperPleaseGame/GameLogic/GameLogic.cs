﻿// <copyright file="GameLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PaperPleaseGame
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;

    /// <summary>
    /// GameLogic.
    /// </summary>
    public class GameLogic
    {
        // Class GameLogic can change the GameModel
        private GameModel gameModel;
        private EventHandler eventHandler;
        private bool gameLost = false;
        private bool gameWin = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameLogic"/> class.
        /// GameLogic cto.
        /// </summary>
        /// <param name="model">GameModel.</param>
        public GameLogic(GameModel model)
        {
            this.gameModel = model;
        }

        /// <summary>
        /// <see cref="EventHandler"/>Gets or sets .
        /// </summary>
        public EventHandler EventHandler { get => this.eventHandler; set => this.eventHandler = value; }

        /// <summary>
        /// <see cref="GameModel"/>Gets or sets .
        /// </summary>
        public GameModel GameModel { get => this.gameModel; set => this.gameModel = value; }

        /// <summary>
        /// Gets or sets a value indicating whether gameFinished or not.
        /// </summary>
        public bool GameLost { get => this.gameLost; set => this.gameLost = value; }

        /// <summary>
        /// Gets or sets a value indicating whether gameWin or not.
        /// </summary>
        public bool GameWin { get => this.gameWin; set => this.gameWin = value; }

        /// <summary>
        /// check if the NPC have the right to pass.
        /// </summary>
        /// <param name="immigrant">NPC.</param>
        /// <returns>boolean.</returns>
        public bool AllowedToPass(Immigrant immigrant)
        {
            if (this.gameModel.Day == 0)
            {
                if (immigrant.ID == null || immigrant.Passport == null)
                {
                    return false;
                }
                else if (immigrant.ContrabandOwnership)
                {
                    return false;
                }
                else if (immigrant.ID.Name != immigrant.Passport.Name || immigrant.ID.PersonId != immigrant.Passport.PersonId
                    || immigrant.ID.Country != immigrant.Passport.Country || immigrant.ID.PersonId == null || immigrant.Passport.PersonId == null)
                {
                    return false;
                }
                else if (immigrant.ID.ImageFaces != immigrant.Passport.ImageFaces
                    || immigrant.ID.ImageFaces != immigrant.ImageFaces
                    || immigrant.ImageFaces != immigrant.Passport.ImageFaces)
                {
                    return false;
                }
                else if (immigrant.ID.Country != "Imperium Romanum" && immigrant.Passport.Country != "Imperium Romanum")
                {
                    return false;
                }
                else if (immigrant.Passport.ExpiredDate < this.gameModel.DateTimeToday || immigrant.ID.ExpiredDate < this.gameModel.DateTimeToday)
                {
                    return false;
                }
            }
            else if (this.gameModel.Day == 1)
            {
                if (immigrant.ContrabandOwnership)
                {
                    return false;
                }
                else if (immigrant.ID.Name != immigrant.Passport.Name || immigrant.ID.PersonId != immigrant.Passport.PersonId
                    || immigrant.ID.Country != immigrant.Passport.Country)
                {
                    return false;
                }
                else if (immigrant.ID.ImageFaces != immigrant.Passport.ImageFaces
                    || immigrant.ID.ImageFaces != immigrant.ImageFaces
                    || immigrant.ImageFaces != immigrant.Passport.ImageFaces)
                {
                    return false;
                }
                else if (this.gameModel.Countries.Count(x => x == immigrant.ID.Country) == 0 || this.gameModel.Countries.Count(x => x == immigrant.Passport.Country) == 0)
                {
                    return false;
                }
                else if (immigrant.ID.Ethnic == "Gypsi" || immigrant.Passport.Ethnic == "Gypsi")
                {
                    return false;
                }
                else if (immigrant.Passport.ExpiredDate < this.gameModel.DateTimeToday || immigrant.ID.ExpiredDate < this.gameModel.DateTimeToday)
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Check Game Lost Or Not by the money or if till the day 2 and the player havent handle all the NPC in the list.
        /// </summary>
        /// <returns>finished game.</returns>
        public bool CheckGameLostOrNot()
        {
            return this.gameLost = this.gameModel.Money <= -1 ||
                (this.gameModel.QueueOfImmigrants.IndexOf(this.gameModel.CurrentImmigrant) <
                this.gameModel.QueueOfImmigrants.Count() - 1 && this.gameModel.Day == 2);
        }

        /// <summary>
        /// Check Game Win Or Not by the money or if till the day 2 and the player have handle all the NPC in the list.
        /// </summary>
        /// <returns>finished game.</returns>
        public bool CheckGameWin()
        {
            return this.gameWin = this.gameModel.Money >= 0 &&
                (this.gameModel.QueueOfImmigrants.IndexOf(this.gameModel.CurrentImmigrant) ==
                this.gameModel.QueueOfImmigrants.Count() - 1 && this.gameModel.Day == 2);
        }

        /// <summary>
        /// Deny The NPC.
        /// </summary>
        public void DenyThem()
        {
            int element = this.gameModel.QueueOfImmigrants.IndexOf(this.gameModel.CurrentImmigrant);
            if (element + 1 < this.gameModel.QueueOfImmigrants.Count())
            {
                bool allowedToPassOrNot
                = this.AllowedToPass(this.gameModel.CurrentImmigrant);
                if (allowedToPassOrNot == true)
                {
                    this.gameModel.Money = this.gameModel.Money - 5;
                }
                else if (allowedToPassOrNot == false)
                {
                    this.gameModel.Money++;
                }

                this.CheckGameLostOrNot();
                this.CheckGameWin();
                this.gameModel.CurrentImmigrant = this.gameModel.QueueOfImmigrants[element + 1];
            }
        }

        /// <summary>
        /// Allow the NPC.
        /// </summary>
        public void AllowThem()
        {
            int element = this.gameModel.QueueOfImmigrants.IndexOf(this.gameModel.CurrentImmigrant);
            if (element + 1 < this.gameModel.QueueOfImmigrants.Count())
            {
                bool allowedToPassOrNot = this.AllowedToPass(this.gameModel.CurrentImmigrant);
                if (allowedToPassOrNot == true)
                {
                    this.gameModel.Money++;
                }
                else if (allowedToPassOrNot == false)
                {
                    this.gameModel.Money = this.gameModel.Money - 5;
                }

                this.CheckGameLostOrNot();
                this.CheckGameWin();
                this.gameModel.CurrentImmigrant = this.gameModel.QueueOfImmigrants[element + 1];
            }
        }
    }
}
