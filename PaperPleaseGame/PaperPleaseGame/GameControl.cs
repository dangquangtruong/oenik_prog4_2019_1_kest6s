﻿// <copyright file="GameControl.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PaperPleaseGame
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Threading;

    /// <summary>
    /// GameControl class, WPF GameWindow will use this one directly.
    /// </summary>
    public class GameControl : FrameworkElement
    {
        private GameModel gameModel;
        private GameDisplay gameDisplay;
        private GameLogic gameLogic;
        private DispatcherTimer dispatcherTimer;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameControl"/> class.
        /// </summary>
        public GameControl()
        {
            this.Loaded += this.GameControl_Loaded;
        }

        /// <summary>
        /// OnRender override.
        /// </summary>
        /// <param name="drawingContext">DrawingContext.</param>
        protected override void OnRender(DrawingContext drawingContext)
        {
            if (this.gameDisplay != null)
            {
                this.gameDisplay.DrawingImages(drawingContext);
            }
        }

        private void GameControl_Loaded(object sender, RoutedEventArgs e)
        {
            this.gameModel = new GameModel();
            this.gameLogic = new GameLogic(this.gameModel);
            this.gameDisplay = new GameDisplay(this.gameModel);
            Window window = Window.GetWindow(this);
            if (window != null)
            {
                this.dispatcherTimer = new DispatcherTimer();
                this.dispatcherTimer.Interval = TimeSpan.FromMilliseconds(300000); // 1 day in this game = 300s = 5 minutes;
                this.dispatcherTimer.Tick += this.DispatcherTimer_Tick;
                this.dispatcherTimer.Start();
                window.KeyDown += this.Window_KeyDown;

                if (this.gameLogic.GameModel.Day == 0)
                {
                    MessageBox.Show("Day 0: Only the people from our country, glorious IMPERIUM ROMANUM can cross the border");
                }

                if (this.gameLogic.GameModel.Day == 1)
                {
                    MessageBox.Show("Day 1: People from the authentic country can cross the border, except those peski Gypsi ethnic");
                }
            }

            this.InvalidateVisual();
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Right: this.gameLogic.AllowThem();
                    if (this.gameLogic.GameLost == true)
                    {
                        MessageBox.Show("Loser, You are Sacked..... Here is the account: " + this.gameLogic.GameModel.Money);
                    }

                    break;
                case Key.Left: this.gameLogic.DenyThem();
                    if (this.gameLogic.GameLost == true)
                    {
                        MessageBox.Show("Loser, You are Sacked..... Here is the account: " + this.gameLogic.GameModel.Money);
                    }

                    break;
            }

            this.InvalidateVisual();
        }

        private void DispatcherTimer_Tick(object sender, EventArgs e)
        {
            if (this.gameLogic.GameModel.Day < 2)
            {
                this.gameLogic.GameModel.Day++;
                this.gameLogic.GameModel.DateTimeToday = this.gameLogic.GameModel.DateTimeToday.Add(TimeSpan.FromDays(1));
            }

            this.InvalidateVisual();
        }
    }
}
