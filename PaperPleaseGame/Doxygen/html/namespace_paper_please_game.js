var namespace_paper_please_game =
[
    [ "Properties", "namespace_paper_please_game_1_1_properties.html", "namespace_paper_please_game_1_1_properties" ],
    [ "App", "class_paper_please_game_1_1_app.html", "class_paper_please_game_1_1_app" ],
    [ "Document", "class_paper_please_game_1_1_document.html", "class_paper_please_game_1_1_document" ],
    [ "GameControl", "class_paper_please_game_1_1_game_control.html", "class_paper_please_game_1_1_game_control" ],
    [ "GameDisplay", "class_paper_please_game_1_1_game_display.html", "class_paper_please_game_1_1_game_display" ],
    [ "GameLogic", "class_paper_please_game_1_1_game_logic.html", "class_paper_please_game_1_1_game_logic" ],
    [ "GameModel", "class_paper_please_game_1_1_game_model.html", "class_paper_please_game_1_1_game_model" ],
    [ "GameWindow", "class_paper_please_game_1_1_game_window.html", "class_paper_please_game_1_1_game_window" ],
    [ "ID_Card", "class_paper_please_game_1_1_i_d___card.html", "class_paper_please_game_1_1_i_d___card" ],
    [ "Immigrant", "class_paper_please_game_1_1_immigrant.html", "class_paper_please_game_1_1_immigrant" ],
    [ "MainWindow", "class_paper_please_game_1_1_main_window.html", "class_paper_please_game_1_1_main_window" ],
    [ "Passport", "class_paper_please_game_1_1_passport.html", "class_paper_please_game_1_1_passport" ]
];