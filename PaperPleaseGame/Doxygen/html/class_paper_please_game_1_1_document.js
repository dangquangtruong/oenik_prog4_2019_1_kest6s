var class_paper_please_game_1_1_document =
[
    [ "Document", "class_paper_please_game_1_1_document.html#a226bf19550df35aa66970309740a83b6", null ],
    [ "GetNPCBrush", "class_paper_please_game_1_1_document.html#aba05a0680bb1797c4edbcbad2a3f1caa", null ],
    [ "Country", "class_paper_please_game_1_1_document.html#ab7835300ca09487eeeb80115a317a2c9", null ],
    [ "DateOfBirth", "class_paper_please_game_1_1_document.html#a26282982656c3fd3effcf7c688c174d6", null ],
    [ "DocumentRect", "class_paper_please_game_1_1_document.html#af74f1caf2ff65a295f027829d0c99725", null ],
    [ "Ethnic", "class_paper_please_game_1_1_document.html#a165f09cdf4c0ca26754eacd1563d40b9", null ],
    [ "ExpiredDate", "class_paper_please_game_1_1_document.html#abdb331c932964740e781032ce77908d1", null ],
    [ "Gender", "class_paper_please_game_1_1_document.html#a78c964a68a8c0edaed4b3524d901bbd8", null ],
    [ "ImageFaces", "class_paper_please_game_1_1_document.html#a32d1108c707a35e08ea46417fd0f7b28", null ],
    [ "Name", "class_paper_please_game_1_1_document.html#aa12fa5da32e511f424a6ef82ffebf7fb", null ],
    [ "PersonId", "class_paper_please_game_1_1_document.html#a3f2995cbf9ae750fa5eaea068a75aa02", null ]
];