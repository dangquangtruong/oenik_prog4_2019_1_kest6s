var class_paper_please_game_1_1_game_logic =
[
    [ "GameLogic", "class_paper_please_game_1_1_game_logic.html#a130038ac66cdd45f3e496dfc2ee58ef5", null ],
    [ "AllowedToPass", "class_paper_please_game_1_1_game_logic.html#a4cf19c7fa1ebb1531f571361ad67162c", null ],
    [ "AllowThem", "class_paper_please_game_1_1_game_logic.html#a9ba1c3e4193264b8c2726210d6b64539", null ],
    [ "CheckGameLostOrNot", "class_paper_please_game_1_1_game_logic.html#a0f73e3d9d7e01ed5c547e99e6845d947", null ],
    [ "CheckGameWin", "class_paper_please_game_1_1_game_logic.html#ae31867f5dfa8300a21bb92292eccf211", null ],
    [ "DenyThem", "class_paper_please_game_1_1_game_logic.html#a89582edfb3ebc73d7f6a1236754146a1", null ],
    [ "EventHandler", "class_paper_please_game_1_1_game_logic.html#a78e477b4ac9cb03fab3e648cf930fd18", null ],
    [ "GameLost", "class_paper_please_game_1_1_game_logic.html#ab48a70e87db6926896df6644675ad33a", null ],
    [ "GameModel", "class_paper_please_game_1_1_game_logic.html#a73a054c8bc13201c16003153dd797c05", null ],
    [ "GameWin", "class_paper_please_game_1_1_game_logic.html#a91ac4ac486fc84b22786034409836334", null ]
];