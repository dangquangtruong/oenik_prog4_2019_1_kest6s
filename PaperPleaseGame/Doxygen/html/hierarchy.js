var hierarchy =
[
    [ "Application", null, [
      [ "PaperPleaseGame.App", "class_paper_please_game_1_1_app.html", null ],
      [ "PaperPleaseGame.App", "class_paper_please_game_1_1_app.html", null ],
      [ "PaperPleaseGame.App", "class_paper_please_game_1_1_app.html", null ]
    ] ],
    [ "ApplicationSettingsBase", null, [
      [ "PaperPleaseGame.Properties.Settings", "class_paper_please_game_1_1_properties_1_1_settings.html", null ]
    ] ],
    [ "PaperPleaseGame.Document", "class_paper_please_game_1_1_document.html", [
      [ "PaperPleaseGame.ID_Card", "class_paper_please_game_1_1_i_d___card.html", null ],
      [ "PaperPleaseGame.Passport", "class_paper_please_game_1_1_passport.html", null ]
    ] ],
    [ "FrameworkElement", null, [
      [ "PaperPleaseGame.GameControl", "class_paper_please_game_1_1_game_control.html", null ]
    ] ],
    [ "PaperPleaseGame.GameDisplay", "class_paper_please_game_1_1_game_display.html", null ],
    [ "PaperPleaseGame.GameLogic", "class_paper_please_game_1_1_game_logic.html", null ],
    [ "PaperPleaseGame.GameModel", "class_paper_please_game_1_1_game_model.html", null ],
    [ "IComponentConnector", null, [
      [ "PaperPleaseGame.GameWindow", "class_paper_please_game_1_1_game_window.html", null ],
      [ "PaperPleaseGame.GameWindow", "class_paper_please_game_1_1_game_window.html", null ],
      [ "PaperPleaseGame.MainWindow", "class_paper_please_game_1_1_main_window.html", null ],
      [ "PaperPleaseGame.MainWindow", "class_paper_please_game_1_1_main_window.html", null ]
    ] ],
    [ "PaperPleaseGame.Immigrant", "class_paper_please_game_1_1_immigrant.html", null ],
    [ "InternalTypeHelper", null, [
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ],
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ]
    ] ],
    [ "Test.Program", "class_test_1_1_program.html", null ],
    [ "PaperPleaseGame.Properties.Resources", "class_paper_please_game_1_1_properties_1_1_resources.html", null ],
    [ "Window", null, [
      [ "PaperPleaseGame.GameWindow", "class_paper_please_game_1_1_game_window.html", null ],
      [ "PaperPleaseGame.GameWindow", "class_paper_please_game_1_1_game_window.html", null ],
      [ "PaperPleaseGame.GameWindow", "class_paper_please_game_1_1_game_window.html", null ],
      [ "PaperPleaseGame.MainWindow", "class_paper_please_game_1_1_main_window.html", null ],
      [ "PaperPleaseGame.MainWindow", "class_paper_please_game_1_1_main_window.html", null ],
      [ "PaperPleaseGame.MainWindow", "class_paper_please_game_1_1_main_window.html", null ]
    ] ]
];